# Use an official Python runtime as a parent image
FROM python:3.9

# Set the working directory in the container
WORKDIR /app

#WORKDIR /app: Sets the working directory inside the container to /app, which is where we'll copy all our project files.

# Copy the Python dependencies file to the working directory
COPY backend/requirements.txt .

# Install Python dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy all backend files to the working directory
COPY backend .

# Expose the port number that FastAPI will listen on
EXPOSE 8000

# Command to run the FastAPI application
CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "8000"]
