from fastapi import FastAPI, HTTPException
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel, Field
from fastapi.templating import Jinja2Templates
from fastapi import Request, APIRouter, HTTPException
from fastapi.responses import HTMLResponse
from decimal import *
from decimal import Decimal, getcontext, ROUND_HALF_UP

app = FastAPI()


# Mount the static directory to serve static files
app.mount("/static", StaticFiles(directory="static"), name="static")

# Define templates for rendering HTML
templates = Jinja2Templates(directory="static")
# Define APIRouter
router = APIRouter()


# Define route for rendering HTML
@router.get("/", response_class=HTMLResponse, operation_id="read_root")
async def read_root(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

# Include router in main app
app.include_router(router)


# also if i build an ui need to do validation there also.
# monthlyAmount: #step of 100 int
# yearlyCAGR: #step of 0.1 %    
# numberOfYears: #step of 1 int
# inflationRate: # step of 0.1 %
# expenseRatio: #step of 0.05 % 

# Validation for input request
class InputData(BaseModel):
    monthlyAmount: int = Field(..., ge=0, multiple_of=100, description="Step of 100 for monthly amount")  # Positive integer, multiple of 100
    yearlyCAGR: float = Field(..., ge=0, description="Non-negative yearly CAGR percentage")  # Non-negative float representing a percentage
    numberOfYears: int = Field(..., gt=0, description="Non-negative number of years")  # Non-negative Positive integer
    inflationRate: float = Field(..., ge=0, description="Non-negative inflation rate percentage")  # Non-negative float representing a percentage
    expenseRatio: float = Field(..., ge=0, description="Non-negative expense ratio percentage")  # Non-negative float representing a percentage   


def calculate_future_value_without_expenseratio_and_inflation(P, I, N, T):

    # Set decimal precision # Adjust precision as needed
    getcontext().prec = 10  

    # Convert inputs to Decimal
    P = Decimal(P)
    i = Decimal(I)
    n = Decimal(N)
    t = Decimal(T)    

    # Calculate future value without considering the expense ratio and inflation
    FV = P * (((1 + i) ** (n * t) - 1) / i) * (1 + i)

    return FV

def calculate_future_value_with_expense_ratio(P,I,N,T,ER):
    # Set decimal precision
    getcontext().prec = 10  

    # Convert inputs to Decimal
    P = Decimal(P)
    i = Decimal(I)
    n = Decimal(N)
    t = Decimal(T)
    ER = Decimal(ER)

    # Calculate future value considering the expense ratio
    FV = P * (((1 + i) ** (n * t) - 1) / i) * (1 + i)

    # Calculate daily expense impact and subtract it for each day
    for day in range(365 * int(t)):
        daily_expense_impact = FV * ER / 365
        FV -= daily_expense_impact

    return FV

def calculate_future_value_with_expenseratio_and_inflation(P, I, N, T, ER, IR):
    # Set decimal precision
    getcontext().prec = 10  # Adjust precision as needed

    # Convert inputs to Decimal
    P = Decimal(P)
    i = Decimal(I)
    n = Decimal(N)
    t = Decimal(T)
    ER = Decimal(ER)
    IR = Decimal(IR)

    # Calculate future value considering both expense ratio and inflation
    FV = P * (((1 + i) ** (n * t) - 1) / i) * (1 + i)

    # Calculate daily expense impact and subtract it for each day
    for day in range(365 * int(t)):
        daily_expense_impact = FV * ER / 365
        FV -= daily_expense_impact

    # Adjust for inflation
    FV *= (1 - IR)

    return FV

# Define a root endpoint
@app.get('/')
def read_root():
    return {"message": "Welcome to the SIP calculator API"}

@app.post('/calculate')
def calculate_sip_future_value(data: InputData):
    # Extract input parameters from the JSON payload received from the frontend
    P = data.monthlyAmount
    I = (data.yearlyCAGR/100) / 12  # Convert yearly CAGR to monthly
    N = 12 # Number of compounding periods per year
    T = data.numberOfYears  #number of years
    ER = (data.expenseRatio/100) #expense ratio of fund/asset
    IR = (data.inflationRate/100) #avg inflation rate of india 

    # Calculate future values based on the input parameters   
    FV_without_expense_and_inflation = calculate_future_value_without_expenseratio_and_inflation(P,I,N,T)    
    FV_with_expense = calculate_future_value_with_expense_ratio(P,I,N,T,ER)
    FV_with_expense_and_inflation = calculate_future_value_with_expenseratio_and_inflation(P,I,N,T,ER,IR)

    # Prepare response as JSON to send back to the frontend
    response = {
        'FutureValue_Without-Expense_ratio-Inflation-Crore': (FV_without_expense_and_inflation / Decimal('10000000')).quantize(Decimal('0.001'), rounding=ROUND_HALF_UP),
        'FutureValue_With-Expense_ratio-Crore': (FV_with_expense / Decimal('10000000')).quantize(Decimal('0.001'), rounding=ROUND_HALF_UP),
        'FutureValue_With-Expense_ratio-Inflation-Crore': (FV_with_expense_and_inflation / Decimal('10000000')).quantize(Decimal('0.001'), rounding=ROUND_HALF_UP)
    }

    return response