function calculateSIP() {
    // Get input values from the form
    const monthlyAmount = document.getElementById('monthlyAmount').value;
    const yearlyCAGR = document.getElementById('yearlyCAGR').value;
    const numberOfYears = document.getElementById('numberOfYears').value;
    const inflationRate = document.getElementById('inflationRate').value;
    const expenseRatio = document.getElementById('expenseRatio').value;

    // Prepare data for the API request
    const requestData = {
        monthlyAmount: parseInt(monthlyAmount),
        yearlyCAGR: parseFloat(yearlyCAGR),
        numberOfYears: parseInt(numberOfYears),
        inflationRate: parseFloat(inflationRate),
        expenseRatio: parseFloat(expenseRatio)
    };

    // Make a POST request to the backend API
    fetch('/calculate', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(requestData)
    })
    .then(response => response.json())
    .then(data => {
        console.log('Response Data:', data); // Log response data to console
        // Update the UI with the calculated results
        const resultElement = document.getElementById('result');
        resultElement.innerHTML = `
            <p>Future Value without Expense and Inflation in Crore: ${data['FutureValue_Without-Expense_ratio-Inflation-Crore']}</p>
            <p>Future Value with Expense in Crore: ${data['FutureValue_With-Expense_ratio-Crore']}</p>
            <p>Future Value with Expense and Inflation in Crore: ${data['FutureValue_With-Expense_ratio-Inflation-Crore']}</p>
        `;
    })
    .catch(error => {
        console.error('Error:', error);
    });


}


